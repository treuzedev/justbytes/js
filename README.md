# justbytes

JS implementation of JustBytes.

<br>

## Table of Contents

* [Why this module? How to use it?](#why-this-module-?-how-to-use-it-?)
* [Specs](#specs)
* [Installation](#installation)
* [How To](#how-to)
* [Functions](#functions)
  * [sendFile](#sendfile)
    * [Arguments](#arguments)
  * [sendFileFromTag](#sendfilefromtag)
    * [Arguments](#arguments-1)
* [Complete Workflow](#complete-workflow)
  * [Client](#client-browser)
  * [Server](#server-nodejs)
* [CI/CD](#cicd)

<br>

## Why this module? How to use it?

This module exists to send a file (receiving is in development), any file (the extensions tested anyway), from point A to point B.

It works best if used in conjunction with [this](https://www.npmjs.com/package/@treuzedev/justbytes) NPM package.

Once a file is choosen, the module breaks it into base 64 encoded strings of raw bytes. These chunks are sent to the specified endpoint. The NPM package has a function, `saveMultipartFile`, that assembles everything and saves the file to a path specified by this module once all chunks are received. See [here](#complete-workflow) for a complete workflow.

<br>

## Specs

Current deployed versions in the CDN (master branch should, but is not necessarily, the latest deployed version - it can be a WIP; the Git tag always matches with the deployed versions in the CDN though):
* `0.2.1`, `0.2.0`

<br>

Tested in:
* Chrome:
  * Version 90.0.4430.93 (Official Build) (64-bit)
* Brave:
  * Version 1.24.82 Chromium: 90.0.4430.93 (Official Build) (64-bit)

<br>

Note that this module, for now, can only send files with one extension safely, ie, trying to send files with extensions like `.tar.gz` may result in corrupted files. An option to specify the filename and the extension may de added in the future.

Extensions tested:
* gif, jpg, png
* json, pdf, txt

<br>

Operating Systems tested:
* Amazon Linux 2
* Windows 10

<br>

## Installation

The module is available at `https://cdn.treuze.dev/js/main.js`.

Adding a script tag, like so - `<script type="text/javascript" src="https://cdn.treuze.dev/js/justbytes.js"></script>` - adds the latest version of the module to your code.

If you want a specific version, then simply change the file name, for example, to `justbytes-0.1.1.js`.

You can then use every function defined in the file, although you should only concern yourself with two of them, `sendFile` and `sendFileFromTag`.

Alternatively, you can also get it from this repo.

<br>

## How to

Send a `File` from an input tag ([docs](#sendfilefromtag)):

```
<html>

  <head>
    <title>JustBytes Testing</title>
  </head>

  <body>

    <!-- lets a user select a file -->
    <input id="input" type="file" />

    <!-- JS -->
    <script type="text/javascript" src="https://cdn.treuze.dev/js/main.js"></script>
    <script>
        
        const testEndpoint = 'http://52.213.42.144:5000/save-file';
        sendFileFromTag({
          'input': document.getElementById('input'),
          'url': testEndpoint,
        });
        
    </script>

  </body>

</html>
```

<br>

Send a `File`or a `Blob` ([docs](#sendfile)):

```
<html>

  <head>
    <title>JustBytes Testing</title>
  </head>

  <body>

    <!-- lets a user select a file -->
    <input id="input" type="file" />

    <!-- JS -->
    <script type="text/javascript" src="https://cdn.treuze.dev/js/main.js"></script>
    <script>
        
        const input = document.getElementById('input');
        const testEndpoint = 'http://52.213.42.144:5000/save-file';
        
        input.onchange = async () => {
          sendFile({
            'input': input['files'][0],
            'url': testEndpoint,
            'finalDestination': '/home/ec2-user/environment',
            'chunkSize': 1024,
            'bytesPrefix': 'b',
            'syncSend': false,
          });
        };
        
    </script>

  </body>

</html>
```

<br>

## Functions

<br>

### sendFile

Sends a `File` or a `Blob` JS object.

Returns a Promise that resolves to an object indicating if upload was successful or not (all chunks may be sent successfully, but an error may occur in the backend). If something goes wrong, it throws an error instead.

Throws an error if:
* The JSON object containing the parameters is not valid
* If there is a problem processing or sending the `File` / `Blob`

Pay attention that the function receives one and only one argument, an object containing some keys. Some of those keys are not required and set by default. Note that if the object has keys other than the ones accepted, it will throw an error.

<br>

```
await sendFile({
  'input': file,
  'url': endpoint,
  'finalDestination': dir,
  'chunkSize': chunkSize,
  'bytesPrefix': bytesPrefix,
  'syncSend': syncSend,
});
```

<br>

#### Arguments

<br>

`input`

Required.

A File or Blob JS object.

<br>

`url`

Required.

The endpoint where the file is sent to. Needs to be of type `string`.

<br>

`chunkSize` and `bytesPrefix:`

The size of the chunk that is sent.

chunkSize is an integer, and bytesPrefix, a `string`, should be on of `kb` or `b`.

Defaults to `1024` and `b`, respectively.

If the resulting `chunkSize` is bigger than the file actual size, only one chunk is sent.

<br>

`syncSend:`

If syncSend is true, then the function only sends a package after the previous one has been successfully sent. Slower but safer, ie, if there is a problem midway, then no extra chunks are sent.

If syncSend is false, then parts are sent even if the previous one hasn't successfully reached the endpoint; file parts may reach the endpoint out of order, or may not reach the endpoint at all; if that happens the file creation will probably fail.

Defaults to `true`.

<br>
 
`finalDestination:`

Where to save the file in the destination endpoint.

Defaults to `/tmp/justbytes`

<br>

### sendFileFromTag

Sends a `File` from an HTML `<input>` tag.

Returns a Promise that resolves to an object indicating if upload was successful or not (all chunks may be sent successfully, but an error may occur in the backend). If something goes wrong, it throws an error instead.

Throws an error if:
* The JSON object containing the parameters is not valid
* If there is a problem processing or sending the `File`

Pay attention that the function receives one and only one argument, an object containing some keys. Some of those keys are not required and set by default. Note that if the object has keys other than the ones accepted, it will throw an error.

<br>

```
await sendFileFromTag({
  'input': tag,
  'url': endpoint,
  'finalDestination': dir,
  'chunkSize': chunkSize,
  'bytesPrefix': bytesPrefix,
  'syncSend': syncSend,
});
```

<br>

#### Arguments

<br>

`input`

Required.

An input HTML tag.

<br>

`url`

Required.

The endpoint where the file is sent to. Needs to be of type `string`.

<br>

`chunkSize` and `bytesPrefix:`

The size of the chunk that is sent.

chunkSize is an integer, and bytesPrefix, a `string`, should be on of `kb` or `b`.

Defaults to `1024` and `b`, respectively.

If the resulting `chunkSize` is bigger than the file actual size, only one chunk is sent.

<br>

`syncSend:`

If syncSend is true, then the function only sends a package after the previous one has been successfully sent. Slower but safer, ie, if there is a problem midway, then no extra chunks are sent.

If syncSend is false, then parts are sent even if the previous one hasn't successfully reached the endpoint; file parts may reach the endpoint out of order, or may not reach the endpoint at all; if that happens the file creation will probably fail.

Defaults to `true`.

<br>
 
`finalDestination:`

Where to save the file in the destination endpoint.

Defaults to `/tmp/justbytes`

<br>

## Complete Workflow

This workflow assumes the use of [this](https://www.npmjs.com/package/@treuzedev/justbytes) NPM package in the backend.

Also assumes that this is for demonstration purposes and not production ready.

All chunks are sent to the server; when the last one arrives, the file is saved in the default location, `/tmp/justbytes`, preserving the original file name.

<br>

#### Client (Browser)

```
<html>

  <head>
    <title>JustBytes Testing</title>
  </head>

  <body>

    <!-- lets a user select a file -->
    <input id="input" type="file" />

    <!-- JS -->
    <script type="text/javascript" src="https://cdn.treuze.dev/js/main.js"></script>
    <script>
        
        const testEndpoint = 'https://subdomain.domain.com/save-file';
        const tag = document.getElementById('input');
        
        sendFileFromTag({
          'input': tag,
          'url': testEndpoint,
          'finalDestination': '/home/ec2-user/environment',
          'syncSend': false,
          'chunkSize': 1024,
          'bytesPrefix': 'kb',
        });
        
    </script>

  </body>

</html>
```

<br>

#### Server (NodeJS)

```
// simple server, not production ready
const express = require('express');
const cors = require('cors');

// in addition to the justbytes package, a parser may be needed to correctly transform the body into a JSON object
const bodyParser = require('body-parser');
const justbytes = require('justbytes');

const app = express();
const port = 80;

app.use(cors());
app.use(bodyParser.json({'limit': '50mb'}));

// a route to send the chunks
app.post('/save-file', (request, response) => {
    const status = justbytes.saveMultipartFile(request.body);
    response.send(status);
});

app.listen(port, () => {
    console.log(`Test app listening on https://subdomain.domain.com`);
});
```

<br>

## CI/CD

CI/CD information can be found [here](https://gitlab.com/treuzedev/justbytes/cicd#js).
